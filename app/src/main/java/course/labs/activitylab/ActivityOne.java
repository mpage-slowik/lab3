package course.labs.activitylab;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class ActivityOne extends Activity {

		// string for logcat documentation
		private final static String TAG = "Lab-ActivityOne";

		public int create_count,start_count,resume_count,pause_count,stop_count,restart_count,destroy_count =0;
		public TextView onC,onSta,onR,onP,onSto,onRe,onD;
		private SharedPreferences prefs;
		//Create 7 counter variables, each corresponding to a different one of the lifecycle callback methods.
		// You will need to increment these variables' values when their corresponding lifecycle methods get called.  
		
		@Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.activity_one);
			getTViews();
			//Log cat print out
			Log.i(TAG, "onCreate called");
			if (savedInstanceState != null) {
				Log.d(TAG, "have state bundle");
				create_count=savedInstanceState.getInt("create");
				start_count=savedInstanceState.getInt("start");
				resume_count=savedInstanceState.getInt("resume");
				pause_count=savedInstanceState.getInt("pause");
				stop_count=savedInstanceState.getInt("stop");
				restart_count=savedInstanceState.getInt("restart");
				destroy_count=savedInstanceState.getInt("destroy");

			}
				prefs = getPreferences(MODE_PRIVATE);

				create_count = prefs.getInt("create", 0);
				start_count = prefs.getInt("create", 0);
				resume_count = prefs.getInt("create", 0);
				pause_count = prefs.getInt("create", 0);
				stop_count = prefs.getInt("create", 0);
				restart_count = prefs.getInt("create", 0);
				destroy_count = prefs.getInt("create", 0);

				create_count ++;
				saveCountInfo();
				Log.d(TAG,String.valueOf(create_count));
				onC.setText(getString(R.string.onCreate) + String.valueOf(create_count));
				onSta.setText(getString(R.string.onStart) + String.valueOf(start_count));
				onP.setText(getString(R.string.onPause) + String.valueOf(pause_count));
				onR.setText(getString(R.string.onResume) + String.valueOf(resume_count));
				onSto.setText(getString(R.string.onStop) + String.valueOf(stop_count));
				onRe.setText(getString(R.string.onRestart) + String.valueOf(restart_count));
				onD.setText(getString(R.string.onDestroy) + String.valueOf(destroy_count));

		}

		private void getTViews(){
			onC = (TextView) findViewById(R.id.create);
			onSta = (TextView) findViewById(R.id.start);
			onP = (TextView) findViewById(R.id.pause);
			onR = (TextView) findViewById(R.id.resume);
			onSto = (TextView) findViewById(R.id.stop);
			onRe = (TextView) findViewById(R.id.restart);
			onD = (TextView) findViewById(R.id.destroy);
		}

		@Override
		public boolean onCreateOptionsMenu(Menu menu) {
			// Inflate the menu; this adds items to the action bar if it is present.
			getMenuInflater().inflate(R.menu.activity_one, menu);
			return true;
		}
		
		// lifecycle callback overrides
		
		@Override
		public void onStart(){
			super.onStart();
			
			//Log cat print out
			Log.i(TAG, "onStart called");
			
			//TODO:
			start_count ++;
			saveCountInfo();

		}

		@Override
		public void onPause(){
			super.onPause();
			setContentView(R.layout.activity_one);

			Log.i(TAG, "onResume called");

			pause_count ++;
			saveCountInfo();

		}

		@Override
		public void onResume(){
			super.onResume();
			setContentView(R.layout.activity_one);

			Log.i(TAG, "onResume called");

			resume_count ++;
			saveCountInfo();

		}
		@Override
		public void onStop(){
			super.onStop();
			setContentView(R.layout.activity_one);

			Log.i(TAG, "onResume called");

			stop_count ++;
			saveCountInfo();

		}


		@Override
		public void onRestart(){
			super.onRestart();
			setContentView(R.layout.activity_one);

			Log.i(TAG, "onRestart called");

			restart_count ++;
			saveCountInfo();

		}
		@Override
		public void onDestroy(){
			super.onDestroy();
			setContentView(R.layout.activity_one);

			Log.i(TAG, "onResume called");

			destroy_count ++;
			saveCountInfo();

		}



		@Override
		public void onSaveInstanceState(Bundle savedInstanceState){
			savedInstanceState.putInt("create",create_count);
			savedInstanceState.putInt("start",start_count);
			savedInstanceState.putInt("resume",resume_count);
			savedInstanceState.putInt("pause",pause_count);
			savedInstanceState.putInt("stop",stop_count);
			savedInstanceState.putInt("restart",restart_count);
			savedInstanceState.putInt("destroy",destroy_count);
			Log.i(TAG,"Saving Instance");
			super.onSaveInstanceState(savedInstanceState);

			// save state information with a collection of key-value pairs
			// save all  count variables
		}
		
		public void launchActivityTwo(View view) {

			Intent launchActivity2 = new Intent(getApplicationContext(),ActivityTwo.class);
			startActivity(launchActivity2);
			// This function launches Activity Two. 
			// Hint: use Context’s startActivity() method.
		}
		public void saveCountInfo(){
			SharedPreferences.Editor editor = prefs.edit();
			Log.i(TAG,"Saving the information to Disk");
			editor.putInt("create",create_count);
			editor.putInt("start",start_count);
			editor.putInt("resume",resume_count);
			editor.putInt("pause",pause_count);
			editor.putInt("stop",stop_count);
			editor.putInt("restart",restart_count);
			editor.putInt("destroy",destroy_count);
			editor.commit();
		}
		

}
